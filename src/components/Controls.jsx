import React, { useEffect, useRef } from 'react';
import { extend, useFrame, useThree } from 'react-three-fiber';

import { OrbitControls } from "three/examples/jsm/controls/OrbitControls";
import { TrackballControls } from "three/examples/jsm/controls/TrackballControls";

extend({ OrbitControls , TrackballControls  });


const Controls = ({ SelectedPlanet }) => {
    


    const {
        camera,
        gl: { domElement }
    } = useThree();

    const controls = useRef()


    if(SelectedPlanet) {

        let coord = SelectedPlanet[0].getWorldPosition()

        controls.current.target.set(coord.x,coord.y,coord.z)
        controls.current.minDistance = 200
        controls.current.maxDistance = 800


    } else if (controls.current){

        controls.current.target.set(0,0,0)
        controls.current.minDistance = 800
        controls.current.maxDistance = Infinity

    }



    useFrame(() =>{ 
        controls.current.update();

    })


    return (
        

        <orbitControls
            ref={controls}
            args={[camera, domElement]}
            enableZoom = {true}
            enablePan = {false}
            enableDamping = {true}
            rotateSpeed = {0.5}
            minDistance={800}
        />

        
    );
}


export default Controls