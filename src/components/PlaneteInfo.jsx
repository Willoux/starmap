import React, { useState } from 'react';
import { useThree } from 'react-three-fiber';

import axios from 'axios';


const PlaneteInfo = ({ Update , SelectedPlanet }) => {

    const DisplayNormal = () => {

        return (
            <>
                <h3>{SelectedPlanet[1].nom}</h3>
                    <div className="p">
                        <p>Rayon : {SelectedPlanet[1].rayon} Km</p>
                        <p>Anneaux : {SelectedPlanet[1].anneaux ? 'non' : 'oui'}</p>
                        <p>Texture : {SelectedPlanet[1].texture}</p>
                    </div>
                <img src={"textures/"+SelectedPlanet[1].texture+".jpg"} alt=""/>
            </>
        )
    }

    const DisplayUpdate = () => {

        return (
            <form>
                <input type="text"/>
                <input type="text"/>
                <input type="text"/>
                <input type="text"/>
            </form>
        )
    }



    return (
        <div className="info">
                            {Update ? <DisplayUpdate /> : <DisplayNormal />}
        </div>

    )
}


export default PlaneteInfo