import React, { useState, useEffect } from "react";
import Menu from './Menu';
import System from './System'


const Map = ({ SystemList , CurrentSystem , setCurrentSystem }) => {

  const Random = (min, max) => {
    return Math.floor(Math.random() * (max - min + 1) + min);
  }

  let teta = []

  for(var i = 0; i < CurrentSystem.planets.length; i++) {
      teta.push(Random(0, Math.PI*2))
      console.log(Random(0, Math.PI*2));
  }


  return (
    <div id="map">
      <System CurrentSystem={CurrentSystem} setCurrentSystem={setCurrentSystem} teta={teta}/> 
      <Menu SystemList={SystemList} CurrentSystem={CurrentSystem} setCurrentSystem={setCurrentSystem}/>
    </div>
  );
}

export default Map;