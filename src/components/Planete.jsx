
import React, { useEffect, useRef, useState } from "react";
import * as THREE from 'three'

import { useLoader, useFrame, useThree } from "react-three-fiber";

const Planete = ({ planete , index , teta , SelectedPlanet , setSelectedPlanet }) => {



    const [play, setPlay] = useState(true)



    useEffect(() => {
        console.log(SelectedPlanet);
        if(SelectedPlanet) {
            if(SelectedPlanet[1].id == planete.id) {
                setPlay(false)
                torus.current.material.opacity[0] = 0
            } else {
                setPlay(true)
                torus.current.material.opacity[0] = 0.1
            }
    
        } else {
            setPlay(true)
            torus.current.material.opacity[0] = 0.1
        }
    }, [SelectedPlanet])


    const handleClick = () => {

        // setPlay(false)
        // torus.current.material.opacity[0] = 0
        setSelectedPlanet([sphere.current, planete])
        // console.log(play);
    }

    const Anneaux = () => {
        
        const anneau = useRef()
        
        return (
            <mesh ref={anneau} rotation={[ Math.PI / 2, 0 , 0]}>
                <torusGeometry args={[planete.rayon*1.5, 15, 2, 70 ]} />
                <meshBasicMaterial color={['ffffff']} opacity={[0.3]} transparent/>
            </mesh>
        )

    }

    const texture = useLoader(THREE.TextureLoader, "./textures/"+planete.texture+".jpg");
    const sphere = useRef()
    const torus = useRef()
    const group = useRef()
    const dist = (index+5)*100
    const materiau = useRef()
    // console.log(materiau.current);

    
    useFrame(() => {

            if(play) {
                sphere.current.rotation.y += 0.002
                group.current.rotation.y += 0.5/dist
            } 
            else {
                sphere.current.rotation.y += 0.0005
            }
        
    })


// console.log(planete.anneaux);
    if(planete.anneaux == "true") {
        console.log('oui');
    }

    return (
        <group ref={group}>
            <mesh ref={sphere} position={[dist*Math.cos(teta), 0, dist*Math.sin(teta)]} onClick={() => handleClick()}>
                <sphereGeometry args={[planete.rayon, 50, 50]}/>
                <meshPhongMaterial ref={materiau} map={texture} />
                {planete.anneaux ? <Anneaux /> : null}
            </mesh>
            <mesh ref={torus} rotation={[ Math.PI / 2, 0 , 0]}>
                <torusGeometry args={[dist, 2, 100, 100]} />
                <meshBasicMaterial color={['fff']} opacity={[0.1]} transparent/>
            </mesh>
        </group>
    )
}


export default Planete