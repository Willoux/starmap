
import React, { Suspense, useRef, useState } from "react";
import * as THREE from 'three'
import Controls from './Controls';
import Planete from './Planete';
import Etoile from './Etoile';
import PlaneteModal from './PlaneteModal'

import { Canvas, useLoader, useFrame, useThree } from "react-three-fiber";


const System = ({ CurrentSystem , setCurrentSystem , teta}) => {

    const [SelectedPlanet, setSelectedPlanet] = useState();

    console.log(SelectedPlanet);


    const Sky = () => {

        // const {
        //     scene
        // } = useThree();

        // const texture = useLoader(THREE.TextureLoader, "./textures/d.jpg")

        // scene.background = texture
        // console.log(scene);
        return null
    }

    
    // #7FFF00

    return (
        <>
            <Canvas id="canvas" camera={{ fov: 60, position: [-3000, 2000, 0], near: 0.1, far: 10000 }} onCreated={({ gl }) => gl.setClearColor(new THREE.Color('#081f2b'))}>
                <Suspense fallback={null}>
                    {/* <Sky /> */}
                    <Etoile etoile={CurrentSystem.star[0]}/>
                    {CurrentSystem.planets.map((planete, index) => {
                        return <Planete planete={planete} index={index} teta={teta[index]} SelectedPlanet={SelectedPlanet} setSelectedPlanet={setSelectedPlanet}/>
                    })}
                </Suspense>
                <Controls SelectedPlanet={SelectedPlanet}/>
                <pointLight position={[0, 0, 0]} intensity={1} color={"#"+CurrentSystem.star[0].couleur}/>
                <ambientLight intensity={0.05}/>
            </Canvas>
            {SelectedPlanet && <PlaneteModal CurrentSystem={CurrentSystem} setCurrentSystem={setCurrentSystem} SelectedPlanet={SelectedPlanet} setSelectedPlanet={setSelectedPlanet}/>}
        </>
    );
}


export default System