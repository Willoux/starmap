import React, { useState } from 'react';
import { useThree } from 'react-three-fiber';

import axios from 'axios';


const PlaneteModal = ({ CurrentSystem , setCurrentSystem , SelectedPlanet , setSelectedPlanet }) => {

    const [Update, setUpdate] = useState(false)

    // const handleClick = () => {
        
    //     console.log(SelectedPlanet);
    // }

    
    const DisplayNormal = () => {
        

        let bool = SelectedPlanet[1].anneaux
        console.log(bool);
        return (
            <div className="info">
                <h3>{SelectedPlanet[1].nom}</h3>
                    <div className="p">
                        <p>Rayon : {SelectedPlanet[1].rayon} Km</p>
                        <p>Texture : {SelectedPlanet[1].texture}</p>
                        <p>Anneaux : {SelectedPlanet[1].anneaux ? 'oui' : 'non'}</p>
                    </div>
                <img src={"textures/"+SelectedPlanet[1].texture+".jpg"} alt=""/>
            </div>
        )
    }

    const DisplayUpdate = () => {

        
        const handleSubmit = (e) => {
            e.preventDefault();

            let anneaux

            console.log(e.target[3].checked);
            if(e.target[3].checked) {
                anneaux = true
            } else {
                anneaux = false 
            }

            axios.put("https://api-space.mehat.etu.mmi-unistra.fr/planet/"+SelectedPlanet[1].id+"?nom="+e.target[0].value+"&rayon="+e.target[1].value+"&texture="+e.target[2].value+"&anneaux="+anneaux)
            .then(() =>
            axios.get('https://api-space.mehat.etu.mmi-unistra.fr/system/'+CurrentSystem.id).then(response => {
                setCurrentSystem(response.data)
            }))
        }

        return (
            <div className="info">
                <form action="" onSubmit={(e) => handleSubmit(e)}>
                    <div className="field">
                        <label htmlFor="nom">Nom de la planète</label>
                        <input type="text" name="nom" placeholder={SelectedPlanet[1].nom} required/>
                    </div>
                    <div className="field">
                        <label htmlFor="rayon">Rayon de la planète</label>
                        <input type="number" name="rayon" placeholder={SelectedPlanet[1].rayon} min="10" max="70" required/>
                    </div>
                    <div className="field">
                        <label htmlFor="texture">Texture de la planète</label>
                        <select name="texture" placeholder={SelectedPlanet[1].texture} required>
                            <option value="gris-brun">gris-brun</option>
                            <option value="gris-clair">gris-clair</option>
                            <option value="gris-fonce">gris-fonce</option>
                            <option value="rouge-brun">rouge-brun</option>
                            <option value="mercure">mercure</option>
                            <option value="venus">venus</option>
                            <option value="terre">terre</option>
                            <option value="mars">mars</option>
                            <option value="jupiter">jupiter</option>
                            <option value="saturne">saturne</option>
                            <option value="uranus">uranus</option>
                            <option value="neptune">neptune</option>
                        </select>
                    </div>
                    <div className="field">
                        <p>Anneaux de la planète</p>
                        <div className="radio">
                            <div>
                                <label htmlFor="anneaux_true">OUI</label>
                                <input type="radio" name="anneaux" value="true" required/>
                            </div>
                            <div>
                                <label htmlFor="anneaux_false">NON</label>
                                <input type="radio" name="anneaux" value="false"/>
                            </div>
                        </div>
                    </div>
                    <input type="submit" value="Valider"/>
                </form>
            </div>
        )
    }

    const handleDelete = () => {
        axios.delete('https://api-space.mehat.etu.mmi-unistra.fr/planet/'+SelectedPlanet[1].id)
        .then(() =>
            axios.get('https://api-space.mehat.etu.mmi-unistra.fr/system/'+CurrentSystem.id).then(response => {
                setCurrentSystem(response.data)
        }))
    }

    return (
        <div className="modal">
            <div className="left">
                {SelectedPlanet && Update ? <DisplayUpdate /> : <DisplayNormal />}
                <div className="options">
                    <div onClick={() => setUpdate(true)}>
                        <img src="img/update.svg" alt="update"/>
                    </div>
                    <div onClick={handleDelete}>
                        <img src="img/delete.svg" alt="delete"/>
                    </div>
                </div>
            </div>
            <div className="right">
                <div className="top" onClick={() => setSelectedPlanet()}>
                    <div className="triangle">
                
                    </div>
                    <div className="block">
                        <div className="close">
                            <div className="first"></div>
                            <div className="second"></div>
                        </div>
                    </div>
                </div>
                <img src="img/planet.svg" alt="planet"/>
            </div>
        </div>
    )
}


export default PlaneteModal