import React, { useRef, useState } from 'react';
import { Link } from "react-router-dom";
import axios from 'axios';

import Search from './Search';


const Menu = ({ SystemList , CurrentSystem , setCurrentSystem }) => {
    
    const [Display, setDisplay] = useState(true)

    const menu = useRef()

    const bouton = useRef()


    const handleClick = () => {
        if(Display) {
            menu.current.style.right = "500px"
            for(let i = 0; i < bouton.current.children.length; i++) {
                bouton.current.children[i].classList.remove("burger"+[i+1])
            }
            setDisplay(false)
        } else {
            menu.current.style.right = "0px"
            for(let i = 0; i < bouton.current.children.length; i++) {
                bouton.current.children[i].classList.add("burger"+[i+1])
            }
            setDisplay(true)
        }
    }

    const handleAdd = () => {

        let planete_id

        axios.post('https://api-space.mehat.etu.mmi-unistra.fr/planet/')
        .then(response => {
            planete_id = response.data.id
            console.log(planete_id);
        }).then(() => 
            axios.put("https://api-space.mehat.etu.mmi-unistra.fr/system/"+CurrentSystem.id+"/planets/"+planete_id)
        ).then(() =>
            axios.get('https://api-space.mehat.etu.mmi-unistra.fr/system/'+CurrentSystem.id).then(response => {
                setCurrentSystem(response.data)
            })
        )
    }

    console.log(CurrentSystem.star);

    return (
        <div className="menu" ref={menu}>
            <div className="left">
            <h1>STAR MAP</h1>
                <div className="search">
                    <div className="icon">
                        <img src="img/loupe.svg" alt="loupe"/>
                    </div>
                    <div className="info">
                        <h3>RECHERCHER</h3>
                        <Search SystemList={SystemList} setCurrentSystem={setCurrentSystem}/>

                        <h3>AJOUTER UN SYSTEME</h3>
                        <div className="bouton"><Link to="/add">+</Link></div>
                    </div>
                </div>
                <div className="options">
                    <h2>{CurrentSystem.nom}</h2>
                    <div className="etoile">
                        <div className="icon">
                            <img src="img/sun.svg" alt="sun"/>
                        </div>
                        <div className="info">
                            <h3>ETOILE</h3>
                                <div className="p">
                                    <p>Nom : {CurrentSystem.star[0].nom}</p>
                                    <p>Rayon : {CurrentSystem.star[0].rayon*1000} Km</p>
                                    <p>Couleur : {CurrentSystem.star[0].couleur}</p>
                                </div>
                        </div>
                    </div>
                    <div className="population">
                        <div className="icon">
                            <img src="img/user.svg" alt="user"/>
                        </div>
                        <div className="info">
                            <h3>POPULATION</h3>
                            <div className="p">
                                <p>Race : {CurrentSystem.race}</p>
                                <p>Population : {CurrentSystem.population}</p>
                            </div>

                        </div>
                    </div>
                    <div className="planete">
                        <div className="icon">
                            <img src="img/planet.svg" alt="planet"/>
                        </div>
                        <div className="info">
                            <h3>PLANETES</h3>
                            <div className="p">
                                <div className="bouton" onClick={() => handleAdd()}>+</div>
                                <p>Nombre : {CurrentSystem.planets.length}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="right">
                <div className="bouton" ref={bouton} onClick={() => handleClick()}>
                    <div className="burger1"></div>
                    <div className="burger2"></div>
                    <div className="burger3"></div>
                </div>
            </div>
        </div>


    );
}


export default Menu