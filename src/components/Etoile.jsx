
import React, { useRef } from "react";
import * as THREE from 'three'

import { useLoader, useFrame } from "react-three-fiber";

const Etoile = ({ etoile }) => {

    let textureB = useLoader(THREE.TextureLoader, "./textures/etoile.jpg");
    
    let textureOther = useLoader(THREE.TextureLoader, "./textures/etoile-blanc.jpg");

    let texture
    
    if(etoile.couleur == "FFFFFF") {
        texture = textureB
    } else {
        texture = textureOther
    }

    const sphere = useRef()

    useFrame(() => {
        if(sphere.current) {
            sphere.current.rotation.y += 0.001
        }

    })
    
    return (
        <mesh ref={sphere}>
            <sphereGeometry args={[etoile.rayon, 50, 50]}/>
            <meshBasicMaterial map={texture} color={"#"+etoile.couleur} />
        </mesh>
    )
}


export default Etoile