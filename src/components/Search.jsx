import React from 'react';

const Search = ({ SystemList , setCurrentSystem }) => {
    
    const handleChange = e => {
        e.preventDefault()
        SystemList.map((system) => {
            if(system.nom === e.target[0].value)    {
                setCurrentSystem(system)
            }
        });
        e.target[0].value = ""
    }


    return (
            <form onSubmit={e => handleChange(e)}>
                <input type="text" list="system-list" placeholder="Nom du systeme..."/>

                <datalist id="system-list">
                {SystemList.map((system) => { return  <option>{system.nom}</option>  })}
                 </datalist>

                <input type="submit" value="ok"></input>
            </form>

    );
}


export default Search