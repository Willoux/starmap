import React, { useState, useEffect } from "react";
import { Switch, Route } from 'react-router-dom';
import axios from 'axios';
import Map from './Map';
import AddSystem from './AddSystem'



const App = () => {

  const [SystemList, setSystemList] = useState([]);
  const [CurrentSystem, setCurrentSystem] = useState()

  useEffect(() => {
    axios.get('https://api-space.mehat.etu.mmi-unistra.fr/system')
     .then(response => {
       setSystemList(response.data);
       setCurrentSystem(response.data[0])
      });
  }, []);


  // useEffect(() => {
  //   axios.get('http://localhost:3000/System.json')
  //    .then(response => {
  //      setSystemList(response.data);
  //      console.log(response.data);
  //      setCurrentSystem(response.data[0])
  //     });
  // }, []);

  return (

      <Switch>
        {CurrentSystem && <Route exact path='/' component={() => <Map SystemList={SystemList} CurrentSystem={CurrentSystem} setCurrentSystem={setCurrentSystem} />} />}
        <Route exact path='/add' component={() => <AddSystem />} />
      </Switch>

        

  );
}

export default App;   