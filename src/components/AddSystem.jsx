import React, { useState, useEffect } from "react";
import axios from 'axios';
import video from '../video/earth.mp4';



const AddSystem = (
  // { SystemList }
  ) => {

//   const [SystemList, setSystemList] = useState([]);
//   const [CurrentSystem, setCurrentSystem] = useState()

//   useEffect(() => {
//     axios.get('http://localhost:3000/System.json')
//      .then(response => {
//        setSystemList(response.data);
//        setCurrentSystem(response.data[0])
//       });
//   }, []);

// console.log(SystemList.length);

  const handleSubmit = (e) => {
      e.preventDefault();
    console.log(e.target[4]);
      let system_id
      let star_id

      const system = axios.post('https://api-space.mehat.etu.mmi-unistra.fr/system?nom='+e.target[0].value+'&race='+e.target[1].value+'&population='+e.target[2].value).then(response => system_id = response.data.id)
      const star = axios.post('https://api-space.mehat.etu.mmi-unistra.fr/star?nom='+e.target[3].value+'&couleur='+e.target[4].value+'&rayon='+e.target[5].value).then(response => star_id = response.data.id)
      
      Promise.all([system, star]).then(() => {
        axios.put("https://api-space.mehat.etu.mmi-unistra.fr/system/"+system_id+"/star/"+star_id)
      })
      .then(() => setTimeout(() => {window.location.href = "https://starmap.mehat.etu.mmi-unistra.fr/"},2000 ))
  }

// const handleSubmitStar = (e) => {
//   e.preventDefault();
//   console.log(e.target[0].value);
//   console.log(e.target[1].value);

//   axios.post('https://api-space.mehat.etu.mmi-unistra.fr/star?id_systeme='+e.target[0].value+'&nom='+e.target[1].value+'&couleur='+e.target[2].value+'&rayon='+e.target[3].value)
//    .then(response => {
//       console.log(response);
//     });
// }


  return (
    <div id="add">
      <div className="window">
      <video autoPlay loop muted>
          <source src={video} type="video/mp4" />
        </video>
        <div className="bg">
          <img src="./textures/d.jpg" alt="bg"/>
          <h1>Ajouter un système</h1>
          <form action="" onSubmit={(e) => handleSubmit(e)}>
            <div className="systeme">
              <div className="nom">
                <label htmlFor="systeme_nom">Nom du système</label>
                <input type="text" name="systeme_nom"/>
              </div>

                <div className="info">
                  <div>
                    <label htmlFor="systeme_race">Race du système</label>
                    <input type="text" name="systeme_race"/>
                  </div>
                  <div>
                    <label htmlFor="systeme_population">Population du système</label>
                    <input type="text" name="systeme_population"/>
                  </div>
                </div>
            </div>

            <div className="etoile">
              <div className="nom">
                <label htmlFor="etoile_nom">Nom de étoile</label>
                <input type="text" name="etoile_nom"/>
              </div>

                <div className="info">
                  <div>
                    <label htmlFor="etoile_couleur">Couleur de l'étoile</label>
                    <select name="etoile_couleur" required>
                      <option value="00BFFF">bleue</option>
                      <option value="7FFF00">verte</option>
                      <option value="FFFFFF">jaune</option>
                    </select>
                  </div>
                  <div>
                    <label htmlFor="etoile_rayon">Rayon de l'étoile</label>
                    <input type="text" name="etoile_rayon"/>
                  </div>
                </div>
            </div>
            <input type="submit" value="Ajouter"/>
          </form>
        </div>
        {/* <div className="deco">

        </div> */}
      </div>

    </div>
  );
}

export default AddSystem;